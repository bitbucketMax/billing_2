﻿namespace Billing
{
    partial class CreateTariffForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtIDTariff = new System.Windows.Forms.TextBox();
            this.txtMinCost = new System.Windows.Forms.TextBox();
            this.txtSMSCost = new System.Windows.Forms.TextBox();
            this.txtMBCost = new System.Windows.Forms.TextBox();
            this.txtProvName = new System.Windows.Forms.TextBox();
            this.cmdCreate = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "id Тарифа";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Стоимость минуты";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Стоимость SMS";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(41, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Стоимость 1 мб";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(37, 123);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Имя провайдера";
            // 
            // txtIDTariff
            // 
            this.txtIDTariff.Location = new System.Drawing.Point(135, 16);
            this.txtIDTariff.Name = "txtIDTariff";
            this.txtIDTariff.Size = new System.Drawing.Size(100, 20);
            this.txtIDTariff.TabIndex = 5;
            // 
            // txtMinCost
            // 
            this.txtMinCost.Location = new System.Drawing.Point(135, 42);
            this.txtMinCost.Name = "txtMinCost";
            this.txtMinCost.Size = new System.Drawing.Size(100, 20);
            this.txtMinCost.TabIndex = 6;
            // 
            // txtSMSCost
            // 
            this.txtSMSCost.Location = new System.Drawing.Point(135, 68);
            this.txtSMSCost.Name = "txtSMSCost";
            this.txtSMSCost.Size = new System.Drawing.Size(100, 20);
            this.txtSMSCost.TabIndex = 7;
            // 
            // txtMBCost
            // 
            this.txtMBCost.Location = new System.Drawing.Point(135, 94);
            this.txtMBCost.Name = "txtMBCost";
            this.txtMBCost.Size = new System.Drawing.Size(100, 20);
            this.txtMBCost.TabIndex = 8;
            // 
            // txtProvName
            // 
            this.txtProvName.Location = new System.Drawing.Point(135, 120);
            this.txtProvName.Name = "txtProvName";
            this.txtProvName.Size = new System.Drawing.Size(100, 20);
            this.txtProvName.TabIndex = 9;
            // 
            // cmdCreate
            // 
            this.cmdCreate.Location = new System.Drawing.Point(135, 199);
            this.cmdCreate.Name = "cmdCreate";
            this.cmdCreate.Size = new System.Drawing.Size(75, 23);
            this.cmdCreate.TabIndex = 10;
            this.cmdCreate.Text = "Создать";
            this.cmdCreate.UseVisualStyleBackColor = true;
            this.cmdCreate.Click += new System.EventHandler(this.cmdCreate_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Location = new System.Drawing.Point(216, 199);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(75, 23);
            this.cmdCancel.TabIndex = 11;
            this.cmdCancel.Text = "Отмена";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // CreateTariffForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 234);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdCreate);
            this.Controls.Add(this.txtProvName);
            this.Controls.Add(this.txtMBCost);
            this.Controls.Add(this.txtSMSCost);
            this.Controls.Add(this.txtMinCost);
            this.Controls.Add(this.txtIDTariff);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "CreateTariffForm";
            this.Text = "CreateTariffForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtIDTariff;
        private System.Windows.Forms.TextBox txtMinCost;
        private System.Windows.Forms.TextBox txtSMSCost;
        private System.Windows.Forms.TextBox txtMBCost;
        private System.Windows.Forms.TextBox txtProvName;
        private System.Windows.Forms.Button cmdCreate;
        private System.Windows.Forms.Button cmdCancel;
    }
}