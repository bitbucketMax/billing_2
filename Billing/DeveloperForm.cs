﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Billing
{
    public partial class DeveloperForm : Form
    {

        public DeveloperForm()
        {
            LogForm logform = this.Owner as LogForm;
            InitializeComponent();
        }

        private void cmdBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Owner.Show();

        }

        private void cmbClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbClass.SelectedIndex == 2)
            {
                label2.Visible = true;
                txtInfo.Visible = true;
                label2.Enabled = true;
                txtInfo.Enabled = true;
                label2.Text = "Введите имя провайдера";
            }
            if (cmbClass.SelectedIndex == 3)
            {
                label2.Visible = true;
                txtInfo.Visible = true;
                label2.Enabled = true;
                txtInfo.Enabled = true;
                label2.Text = "Введите id клиента";
            }
            if ((cmbClass.SelectedIndex == 0) || (cmbClass.SelectedIndex == 1))
            {
                label2.Visible = false;
                txtInfo.Visible = false;
                label2.Enabled = false;
                txtInfo.Enabled = false;
            }
        }

        private void cmdCreate_Click(object sender, EventArgs e)
        {
            if (cmbClass.SelectedIndex == 0)
            {
                CreateTariffForm CreateTariffF = new CreateTariffForm();
                CreateTariffF.Show();
                CreateTariffF.Owner = this;
                this.Hide();
            }
            if (cmbClass.SelectedIndex == 1)
            {
                RegisterForm RegisterF = new RegisterForm();
                RegisterF.Show();
                RegisterF.Owner = this;
                this.Hide();
            }
            if (cmbClass.SelectedIndex == 3)
            {
                ConcludeContractForm ConcludeContractF = new ConcludeContractForm();
                ConcludeContractF.Show();
                ConcludeContractF.Owner = this;
                this.Hide();
            }
        }
    }
}
