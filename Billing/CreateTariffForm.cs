﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Billing
{
    public partial class CreateTariffForm : Form
    {
        public CreateTariffForm()
        {
            InitializeComponent();
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Owner.Show();
        }

        private void cmdCreate_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Owner.Show();
            MessageBox.Show("Объект успешно создан!");
        }

    }
}
