﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Billing
{
    public class Tariff
    {
        public int idtariff,smscost,mincost,mbcost;
        public string nameProv;
 
        public string[] getArr()
        {
            string[] a = {idtariff.ToString(), smscost.ToString(), mincost.ToString(), mbcost.ToString(),nameProv};
            return a;
        }

        public Tariff(int id,int sms,int mincost,int mbcost,string provider)
        {
            this.idtariff = id;
            this.smscost = sms;
            this.mincost = mincost;
            this.mbcost = mbcost;
            this.nameProv = provider;
        }

        
    }
}
