﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Billing
{
    public partial class ChangePInfoForm : Form
    {
        public ChangePInfoForm()
        {
            InitializeComponent();
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {

            this.Hide();
            this.Owner.Show();
        }

        private void ChangePInfoForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void cmdChange_Click(object sender, EventArgs e)
        {
            MainMenu MainMenuForm = this.Owner as MainMenu;
            
            int indexclient = MainMenuForm.logformcopy.control.ExistClient(MainMenuForm.logformcopy.idclient);
          lbresfamilya.Text= MainMenuForm.logformcopy.control.clients[indexclient].ChangeSurname(txtfamilia.Text);
           lbresname.Text= MainMenuForm.logformcopy.control.clients[indexclient].ChangeName(txtname.Text);
           lbresoch.Text= MainMenuForm.logformcopy.control.clients[indexclient].ChangeOtchestvo(txtothestvo.Text);
           lbresadr.Text= MainMenuForm.logformcopy.control.clients[indexclient].ChangeAdress("г. "+txtTown.Text+ ", ул."+txtStreet.Text+", д. "+txtHome.Text);
           lbrespasswd.Text = MainMenuForm.logformcopy.control.clients[indexclient].ChangePassword(txtoldpasswd.Text, txtnewpaswd.Text);
           MainMenuForm.logformcopy.control.Save();
        }
    }
}
