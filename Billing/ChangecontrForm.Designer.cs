﻿namespace Billing
{
    partial class ChangecontrForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listtariff = new System.Windows.Forms.ListView();
            this.lbchangetariffres = new System.Windows.Forms.Label();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.ChtariffBtn = new System.Windows.Forms.Button();
            this.txtpasswd = new System.Windows.Forms.TextBox();
            this.podinternet = new System.Windows.Forms.CheckBox();
            this.podsms = new System.Windows.Forms.CheckBox();
            this.podcall = new System.Windows.Forms.CheckBox();
            this.lbtariff = new System.Windows.Forms.Label();
            this.lbservenabled = new System.Windows.Forms.Label();
            this.lbpasswd = new System.Windows.Forms.Label();
            this.lboldpasswd = new System.Windows.Forms.Label();
            this.txtoldpasswd = new System.Windows.Forms.TextBox();
            this.btnservice = new System.Windows.Forms.Button();
            this.btnpasswd = new System.Windows.Forms.Button();
            this.numphone = new System.Windows.Forms.NumericUpDown();
            this.lbnewnum = new System.Windows.Forms.Label();
            this.lbpasswdchangeres = new System.Windows.Forms.Label();
            this.lbnumchangeres = new System.Windows.Forms.Label();
            this.lbserviceschangeres = new System.Windows.Forms.Label();
            this.btnchangenumb = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numphone)).BeginInit();
            this.SuspendLayout();
            // 
            // listtariff
            // 
            this.listtariff.FullRowSelect = true;
            this.listtariff.Location = new System.Drawing.Point(15, 33);
            this.listtariff.Name = "listtariff";
            this.listtariff.Size = new System.Drawing.Size(377, 186);
            this.listtariff.TabIndex = 21;
            this.listtariff.Tag = "1";
            this.listtariff.UseCompatibleStateImageBehavior = false;
            this.listtariff.View = System.Windows.Forms.View.Details;
            this.listtariff.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listtariff_ColumnClick);
            this.listtariff.SelectedIndexChanged += new System.EventHandler(this.listtariff_SelectedIndexChanged);
            // 
            // lbchangetariffres
            // 
            this.lbchangetariffres.AutoSize = true;
            this.lbchangetariffres.Location = new System.Drawing.Point(413, 76);
            this.lbchangetariffres.Name = "lbchangetariffres";
            this.lbchangetariffres.Size = new System.Drawing.Size(0, 13);
            this.lbchangetariffres.TabIndex = 19;
            // 
            // CancelBtn
            // 
            this.CancelBtn.Location = new System.Drawing.Point(225, 3);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(167, 24);
            this.CancelBtn.TabIndex = 17;
            this.CancelBtn.Text = "Вернуться в меню контракта";
            this.CancelBtn.UseVisualStyleBackColor = true;
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // ChtariffBtn
            // 
            this.ChtariffBtn.Enabled = false;
            this.ChtariffBtn.Location = new System.Drawing.Point(425, 33);
            this.ChtariffBtn.Name = "ChtariffBtn";
            this.ChtariffBtn.Size = new System.Drawing.Size(135, 26);
            this.ChtariffBtn.TabIndex = 18;
            this.ChtariffBtn.Text = "Изменить тариф";
            this.ChtariffBtn.UseVisualStyleBackColor = true;
            this.ChtariffBtn.Click += new System.EventHandler(this.ChtariffBtn_Click);
            // 
            // txtpasswd
            // 
            this.txtpasswd.Location = new System.Drawing.Point(313, 288);
            this.txtpasswd.Name = "txtpasswd";
            this.txtpasswd.Size = new System.Drawing.Size(100, 20);
            this.txtpasswd.TabIndex = 16;
            // 
            // podinternet
            // 
            this.podinternet.AutoSize = true;
            this.podinternet.Location = new System.Drawing.Point(21, 334);
            this.podinternet.Name = "podinternet";
            this.podinternet.Size = new System.Drawing.Size(72, 17);
            this.podinternet.TabIndex = 13;
            this.podinternet.Text = "интернет";
            this.podinternet.UseVisualStyleBackColor = true;
            // 
            // podsms
            // 
            this.podsms.AutoSize = true;
            this.podsms.Location = new System.Drawing.Point(21, 311);
            this.podsms.Name = "podsms";
            this.podsms.Size = new System.Drawing.Size(46, 17);
            this.podsms.TabIndex = 14;
            this.podsms.Text = "смс";
            this.podsms.UseVisualStyleBackColor = true;
            // 
            // podcall
            // 
            this.podcall.AutoSize = true;
            this.podcall.Location = new System.Drawing.Point(21, 288);
            this.podcall.Name = "podcall";
            this.podcall.Size = new System.Drawing.Size(62, 17);
            this.podcall.TabIndex = 15;
            this.podcall.Text = "звонок";
            this.podcall.UseVisualStyleBackColor = true;
            // 
            // lbtariff
            // 
            this.lbtariff.AutoSize = true;
            this.lbtariff.Location = new System.Drawing.Point(12, 9);
            this.lbtariff.Name = "lbtariff";
            this.lbtariff.Size = new System.Drawing.Size(128, 13);
            this.lbtariff.TabIndex = 10;
            this.lbtariff.Text = "Выберите другой тариф";
            // 
            // lbservenabled
            // 
            this.lbservenabled.AutoSize = true;
            this.lbservenabled.Location = new System.Drawing.Point(18, 262);
            this.lbservenabled.Name = "lbservenabled";
            this.lbservenabled.Size = new System.Drawing.Size(122, 13);
            this.lbservenabled.TabIndex = 11;
            this.lbservenabled.Text = "Подключаемые услуги";
            // 
            // lbpasswd
            // 
            this.lbpasswd.AutoSize = true;
            this.lbpasswd.Location = new System.Drawing.Point(230, 288);
            this.lbpasswd.Name = "lbpasswd";
            this.lbpasswd.Size = new System.Drawing.Size(80, 13);
            this.lbpasswd.TabIndex = 12;
            this.lbpasswd.Text = "Новый пароль";
            // 
            // lboldpasswd
            // 
            this.lboldpasswd.AutoSize = true;
            this.lboldpasswd.Location = new System.Drawing.Point(230, 265);
            this.lboldpasswd.Name = "lboldpasswd";
            this.lboldpasswd.Size = new System.Drawing.Size(84, 13);
            this.lboldpasswd.TabIndex = 12;
            this.lboldpasswd.Text = "Старый пароль";
            // 
            // txtoldpasswd
            // 
            this.txtoldpasswd.Location = new System.Drawing.Point(313, 262);
            this.txtoldpasswd.Name = "txtoldpasswd";
            this.txtoldpasswd.Size = new System.Drawing.Size(100, 20);
            this.txtoldpasswd.TabIndex = 16;
            // 
            // btnservice
            // 
            this.btnservice.Location = new System.Drawing.Point(12, 357);
            this.btnservice.Name = "btnservice";
            this.btnservice.Size = new System.Drawing.Size(135, 26);
            this.btnservice.TabIndex = 18;
            this.btnservice.Text = "Изменить список услуг";
            this.btnservice.UseVisualStyleBackColor = true;
            this.btnservice.Click += new System.EventHandler(this.btnservice_Click);
            // 
            // btnpasswd
            // 
            this.btnpasswd.Location = new System.Drawing.Point(425, 258);
            this.btnpasswd.Name = "btnpasswd";
            this.btnpasswd.Size = new System.Drawing.Size(135, 26);
            this.btnpasswd.TabIndex = 18;
            this.btnpasswd.Text = "Изменить пароль";
            this.btnpasswd.UseVisualStyleBackColor = true;
            this.btnpasswd.Click += new System.EventHandler(this.btnpasswd_Click);
            // 
            // numphone
            // 
            this.numphone.Location = new System.Drawing.Point(230, 363);
            this.numphone.Maximum = new decimal(new int[] {
            -204313216,
            20,
            0,
            0});
            this.numphone.Minimum = new decimal(new int[] {
            -1194313216,
            20,
            0,
            0});
            this.numphone.Name = "numphone";
            this.numphone.Size = new System.Drawing.Size(183, 20);
            this.numphone.TabIndex = 23;
            this.numphone.Value = new decimal(new int[] {
            -1194313216,
            20,
            0,
            0});
            // 
            // lbnewnum
            // 
            this.lbnewnum.AutoSize = true;
            this.lbnewnum.Location = new System.Drawing.Point(227, 334);
            this.lbnewnum.Name = "lbnewnum";
            this.lbnewnum.Size = new System.Drawing.Size(76, 13);
            this.lbnewnum.TabIndex = 12;
            this.lbnewnum.Text = "Новый номер";
            // 
            // lbpasswdchangeres
            // 
            this.lbpasswdchangeres.AutoSize = true;
            this.lbpasswdchangeres.Location = new System.Drawing.Point(446, 295);
            this.lbpasswdchangeres.Name = "lbpasswdchangeres";
            this.lbpasswdchangeres.Size = new System.Drawing.Size(0, 13);
            this.lbpasswdchangeres.TabIndex = 19;
            // 
            // lbnumchangeres
            // 
            this.lbnumchangeres.AutoSize = true;
            this.lbnumchangeres.Location = new System.Drawing.Point(422, 397);
            this.lbnumchangeres.Name = "lbnumchangeres";
            this.lbnumchangeres.Size = new System.Drawing.Size(0, 13);
            this.lbnumchangeres.TabIndex = 19;
            // 
            // lbserviceschangeres
            // 
            this.lbserviceschangeres.AutoSize = true;
            this.lbserviceschangeres.Location = new System.Drawing.Point(18, 397);
            this.lbserviceschangeres.Name = "lbserviceschangeres";
            this.lbserviceschangeres.Size = new System.Drawing.Size(0, 13);
            this.lbserviceschangeres.TabIndex = 19;
            // 
            // btnchangenumb
            // 
            this.btnchangenumb.Location = new System.Drawing.Point(425, 358);
            this.btnchangenumb.Name = "btnchangenumb";
            this.btnchangenumb.Size = new System.Drawing.Size(135, 26);
            this.btnchangenumb.TabIndex = 18;
            this.btnchangenumb.Text = "Изменить номер";
            this.btnchangenumb.UseVisualStyleBackColor = true;
            this.btnchangenumb.Click += new System.EventHandler(this.btnchangenumb_Click);
            // 
            // ChangecontrForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 439);
            this.Controls.Add(this.numphone);
            this.Controls.Add(this.listtariff);
            this.Controls.Add(this.lbserviceschangeres);
            this.Controls.Add(this.lbnumchangeres);
            this.Controls.Add(this.lbpasswdchangeres);
            this.Controls.Add(this.lbchangetariffres);
            this.Controls.Add(this.CancelBtn);
            this.Controls.Add(this.btnpasswd);
            this.Controls.Add(this.btnchangenumb);
            this.Controls.Add(this.btnservice);
            this.Controls.Add(this.ChtariffBtn);
            this.Controls.Add(this.txtoldpasswd);
            this.Controls.Add(this.txtpasswd);
            this.Controls.Add(this.podinternet);
            this.Controls.Add(this.podsms);
            this.Controls.Add(this.podcall);
            this.Controls.Add(this.lbtariff);
            this.Controls.Add(this.lbservenabled);
            this.Controls.Add(this.lboldpasswd);
            this.Controls.Add(this.lbnewnum);
            this.Controls.Add(this.lbpasswd);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ChangecontrForm";
            this.Text = "ChangecontrForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ChangecontrForm_FormClosing);
            this.Load += new System.EventHandler(this.ChangecontrForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numphone)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listtariff;
        private System.Windows.Forms.Label lbchangetariffres;
        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.Button ChtariffBtn;
        private System.Windows.Forms.TextBox txtpasswd;
        private System.Windows.Forms.CheckBox podinternet;
        private System.Windows.Forms.CheckBox podsms;
        private System.Windows.Forms.CheckBox podcall;
        private System.Windows.Forms.Label lbtariff;
        private System.Windows.Forms.Label lbservenabled;
        private System.Windows.Forms.Label lbpasswd;
        private System.Windows.Forms.Label lboldpasswd;
        private System.Windows.Forms.TextBox txtoldpasswd;
        private System.Windows.Forms.Button btnservice;
        private System.Windows.Forms.Button btnpasswd;
        private System.Windows.Forms.NumericUpDown numphone;
        private System.Windows.Forms.Label lbnewnum;
        private System.Windows.Forms.Label lbpasswdchangeres;
        private System.Windows.Forms.Label lbnumchangeres;
        private System.Windows.Forms.Label lbserviceschangeres;
        private System.Windows.Forms.Button btnchangenumb;
    }
}