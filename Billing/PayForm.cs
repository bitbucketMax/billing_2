﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Billing
{
    public partial class PayForm : Form
    {
        public PayForm()
        {
            InitializeComponent();
        }



        private void button1_Click(object sender, EventArgs e)
        {
            ContractMenu contractmenu = this.Owner as ContractMenu;
            int indexcontract = contractmenu.MMform.logformcopy.control.ExistContract(contractmenu.MMform.numbcontrselected);
            contractmenu.MMform.logformcopy.control.contracts[indexcontract].ChangeBill(Convert.ToDouble(summ.Value));
            contractmenu.MMform.logformcopy.control.Save();
            this.Owner.Show();
            this.Hide();

        }

        private void PayForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
