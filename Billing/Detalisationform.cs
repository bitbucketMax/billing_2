﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Billing
{
    public partial class Detalisationform : Form
    {
        private string date="дата";
        private string service="сервис";
        private string volume="объем";
        private string nomabon="номер абонента";
        private string cost="стоимость";
        public Detalisationform()
        {
            InitializeComponent();
        }

        private void Detalisationform_Load(object sender, EventArgs e)
        {
            ContractMenu contractmenu = this.Owner as ContractMenu;
            int indexcontract = contractmenu.MMform.logformcopy.control.ExistContract(contractmenu.MMform.numbcontrselected);
            listdetaliz.Columns.Add(date);
            listdetaliz.Columns.Add(service);
            listdetaliz.Columns.Add(volume);
            listdetaliz.Columns.Add(nomabon);
            listdetaliz.Columns.Add(cost);
            int i = 0;
            for (i = 0; i < contractmenu.MMform.logformcopy.control.contracts[indexcontract].events.Count;i++ )
                listdetaliz.Items.Add(new ListViewItem(contractmenu.MMform.logformcopy.control.contracts[indexcontract].events[i].getArr()));
            listdetaliz.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void Detalisationform_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void listdetaliz_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (e.Column != 3)
            {
                ContractMenu contractmenu = this.Owner as ContractMenu;
                int indexcontract = contractmenu.MMform.logformcopy.control.ExistContract(contractmenu.MMform.numbcontrselected);
                List<string[]> levents = new List<string[]>();
                foreach (var strevent in contractmenu.MMform.logformcopy.control.contracts[indexcontract].events)
                    levents.Add(strevent.getArr());
                if (listdetaliz.Tag.Equals("-1"))
                    listdetaliz.Tag = "1";
                else listdetaliz.Tag = "-1";
                levents.Sort(delegate(string[] a1, string[] a2)
                {

                    if ((String.Compare(a1[e.Column], a2[e.Column]) == 1))
                        return (listdetaliz.Tag.Equals("1") ? 1 : -1);
                    else if ((String.Compare(a1[e.Column], a2[e.Column]) == -1))
                        return (listdetaliz.Tag.Equals("1") ? -1 : 1);
                    else return 0;

                });

                listdetaliz.Items.Clear();
                foreach (var strmasevents in levents)
                    listdetaliz.Items.Add(new ListViewItem(strmasevents));
                listdetaliz.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ContractMenu contractmenu = this.Owner as ContractMenu;
            contractmenu.MMform.logformcopy.control.Save();
            
            this.Hide();
            this.Owner.Show();
        }
    }
}
